//
//  CircleView.swift
//  devslopes-social
//
//  Created by Jorge Jimenez on 9/12/16.
//  Copyright © 2016 xadrijo. All rights reserved.
//

import UIKit

class CircleView: UIImageView {
        
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.width / 2
        clipsToBounds = true
    }
}
