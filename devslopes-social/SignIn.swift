//
//  ViewController.swift
//  devslopes-social
//
//  Created by Jorge Jimenez on 9/9/16.
//  Copyright © 2016 xadrijo. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import Firebase


class SignInVC: UIViewController {
    
    @IBOutlet weak var emailField: FancyField!
    @IBOutlet weak var pwdField: FancyField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        if let myKeychain = KeychainWrapper.defaultKeychainWrapper().stringForKey("userId") {
            print("George: ID found in keychain \(myKeychain)")
            performSegue(withIdentifier: "goToFeed", sender: nil)
        }
    }

    
    @IBAction func facebookBtnTapped(_ sender: AnyObject) {
        
        let facebookLogin = FBSDKLoginManager()
        facebookLogin.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if error != nil {
                print("George: Unable to authenticate with Facebook - \(error)")
            } else if result?.isCancelled == true {
                print("George: User cancelled Facebook authentication\n")
            } else {
                print("George: Successfully authenticated with Facebook\n")
                let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
                self.firebaseAuth(credential)
            }
        }
    }
    
    func firebaseAuth(_ credential: FIRAuthCredential) {
        FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
            if error != nil {
                print("George: Unable to authenticate with Firebase - \(error)")
            } else {
                print("George: Successfully authenticated with Firebase")
                
                if let user = user {
                    let userData = ["provider" : credential.provider]
                    
                    let myId: String = "\(user.uid)"
                    print("George \(myId)")
                    
                    self.completedSignIn(id: user.uid, userData: userData)

                }
            }
        })
    }

    @IBAction func signInTapped(_ sender: AnyObject) {
        if let email = emailField.text, let pwd = pwdField.text {
            FIRAuth.auth()?.signIn(withEmail: email, password: pwd, completion: { (user, error) in
                if error == nil {
                    print("George: Email User authenticated with Firebase")
                    if let user = user {
                        let userData = ["provider" : user.providerID]
                        
                        let myId: String = "\(user.uid)"
                        print("George \(myId)")
                        
                        self.completedSignIn(id: user.uid, userData: userData)
                        
                    }
                } else {
                    FIRAuth.auth()?.createUser(withEmail: email, password: pwd, completion: { (user, error) in
                        if error != nil {
                            print("George: Unable to authenticate with Firebase using email")
                        } else {
                            print("George: Successfully authenticated with Firebase")
                        }
                        
                        if let user = user {
                            let userData = ["provider" : user.providerID]
                            
                            let myId: String = "\(user.uid)"
                            print("George \(myId)")
                            
                            self.completedSignIn(id: user.uid, userData: userData)
                            
                        }
                    })
                }
            })
        }
    }
    
    func completedSignIn(id: String, userData: [String : String]) {
        DataService.ds.createFirebaseDBUser(uid: id, userData: userData)
        let isSaved: Bool = KeychainWrapper.defaultKeychainWrapper().setString(id, forKey: KEY_UID)
        
        let myKeychain = KeychainWrapper.defaultKeychainWrapper().stringForKey(KEY_UID)
        print("George: keychain was saved - \(isSaved)")
        print("George: keychain was saved - \(myKeychain)")

        if isSaved {
            performSegue(withIdentifier: "goToFeed", sender: nil)
        }
    }
}




















