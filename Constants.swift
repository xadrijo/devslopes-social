//
//  Constants.swift
//  devslopes-social
//
//  Created by Jorge Jimenez on 9/10/16.
//  Copyright © 2016 xadrijo. All rights reserved.
//

import UIKit

let SHADOW_GREY: CGFloat = 120.0 / 255.0
let KEY_UID = "userId"

